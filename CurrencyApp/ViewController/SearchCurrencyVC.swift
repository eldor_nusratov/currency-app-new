//
//  SearchCurrencyVC.swift
//  CurrencyApp
//
//  Created by eldorbek nusratov on 6/19/20.
//  Copyright © 2020 eldorbek nusratov. All rights reserved.
//

import UIKit

class SearchCurrencyVC: UIViewController {

    
    @IBOutlet weak var searchView: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var currencies : [CurrencyItem] = []
    var searchCurrencies : [CurrencyItem] = []
    var viewmodel : CurrencyViewModel?
    var first : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()


        tableView.delegate = self
        tableView.dataSource = self
        searchView.delegate = self
        currencies = viewmodel?.currencyItems ?? []
        searchCurrencies = currencies
        
    }

    @IBAction func donePressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
}


extension SearchCurrencyVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchCurrencies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = searchCurrencies[indexPath.row].currency
        cell.detailTextLabel?.text = searchCurrencies[indexPath.row].currencyName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if first {
            viewmodel?.setCurrency(currency1: searchCurrencies[indexPath.row], currency2: nil)
        } else {
            viewmodel?.setCurrency(currency1: nil, currency2: searchCurrencies[indexPath.row])
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    
}


extension SearchCurrencyVC : UISearchBarDelegate{
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchCurrencies = currencies
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchCurrencies =  searchCurrencies.filter { (currency) -> Bool in
            return currency.currency.contains(searchBar.text!.uppercased())
        }
        if searchBar.text == "" {
            searchCurrencies = currencies
        }
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}

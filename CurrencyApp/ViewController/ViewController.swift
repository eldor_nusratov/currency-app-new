//
//  ViewController.swift
//  CurrencyApp
//
//  Created by eldorbek nusratov on 6/19/20.
//  Copyright © 2020 eldorbek nusratov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: - Stored Properties
    var viewmodel : CurrencyViewModel = CurrencyViewModel()
    
    @IBOutlet weak var keyboardView: KeyboardView!
    @IBOutlet weak var currencyItemView: CurrencyItemView!
    @IBOutlet weak var exchangeBtn: UIButton!
    
    
    //MARK: - Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewModel()
        exchangeBtn.setShadowCircle()
//        currencyItemView.setShadow(with: 10)
//        currencyItemView.setShadow(with: 5)
    }
    
    func setupViewModel(){
        viewmodel.setupView(currencyItemView: currencyItemView, keyboardView: keyboardView)
    }
    
    @IBAction func currencyPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchCurrencyVC") as! SearchCurrencyVC
        vc.viewmodel = viewmodel
        if sender.tag == 1 {
            vc.first = false
        }
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func keyboardPressed(_ sender: UIButton) {
        if sender.tag < 9{
          viewmodel.quantity += "\(sender.tag+1)"
        } else {
            switch sender.tag {
            case 9:
                print(viewmodel.quantity)
                viewmodel.quantity += "."
                print(viewmodel.quantity)
            case 10:
                viewmodel.quantity += "0"
            case 11:
                viewmodel.quantity.removeLast()
                print(viewmodel.quantity)
            default:
                break
            }
        }
    }
    
    @IBAction func exchangePressed(_ sender: UIButton) {
        viewmodel.exchange()
    }
    
    
    
}


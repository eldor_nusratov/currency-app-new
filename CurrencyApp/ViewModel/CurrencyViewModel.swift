//
//  CurrencyViewModel.swift
//  CurrencyApp
//
//  Created by eldorbek nusratov on 6/19/20.
//  Copyright © 2020 eldorbek nusratov. All rights reserved.
//

import UIKit


class CurrencyViewModel : NSObject{
    
    //MARK: - Stored Properties
    var keyboardView : KeyboardView!
    var currencyItemView : CurrencyItemView!
    
    var manager : NetworkManager = NetworkManager()
    var currencies : [String: Double] = [:]
    var currencyItems : [CurrencyItem] = []
    var currency1 : CurrencyItem?
    var currency2 : CurrencyItem?
    var quantity : String = "0"{
        didSet{
           
            var dots = 0
            print(quantity)
            quantity.forEach { (ch) in
                if ch == "."{
                    dots += 1
                }
            }
            if dots > 1{
                quantity.removeLast()
            }
            
            var interruption = true
            var count = 0
            quantity.forEach { (ch) in
                if ch == "0" && interruption{
                    count += 1
                } else {
                    interruption = false
                }
            }
            if (count > 0){
                quantity.removeFirst(count)
            }
            if quantity.first == "." || quantity.isEmpty{
                quantity = "0" + quantity
                self.updateQuantity()
                return
            }
            self.updateQuantity()
        }
    }
    
    var currencyAmount1 : Double {
        let x = (currencies["USD\(currency2!.currency)"] ?? 0.0) / (currencies["USD\(currency1!.currency)"] ?? 1)
        return Double(round(10000*x)/10000)
    }
    
    var currencyAmount2 : Double {
        let x =  (currencies["USD\(currency1!.currency)"] ?? 0.0) / (currencies["USD\(currency2!.currency)"] ?? 1)
        return Double(round(10000*x)/10000)
    }
    
    //MARK: - Functions
    
    /// Registration collectionView
    func setupView(currencyItemView : CurrencyItemView, keyboardView : KeyboardView) {
        self.keyboardView = keyboardView
        self.currencyItemView = currencyItemView
        list()
//        live()
        repeatLive()
    }
    
    
    func setCurrency(currency1 : CurrencyItem?, currency2 : CurrencyItem?){
        
        if let currency1 = currency1 {
            self.currency1 = currency1
        }
        if let currency2 = currency2 {
            self.currency2 = currency2
        }
        updateCurrency()
    }
    
    func updateQuantity(){
        currencyItemView.currencyPrice1.text = quantity
        var x = Double(quantity) ?? 0.0
        x = x * currencyAmount1
        currencyItemView.currencyPrice2.text = "\(Double(round(10000*x)/10000))"
    }
    
    func updateCurrency(){
        guard let currency1 = currency1, let currency2 = currency2  else {
            return
        }
        
        currencyItemView.currencyName1.text =  currency1.currency + " - " + currency1.currencyName
        currencyItemView.currencyName2.text = currency2.currency + " - " + currency2.currencyName
        currencyItemView.currencySubtitle1.text = "1 \(String(currency1.currency)) - \(currencyAmount1) \(String(currency2.currency))"
        currencyItemView.currencySubtitle2.text = "1 \(String(currency2.currency)) - \(currencyAmount2) \(String(currency1.currency))"
        updateQuantity()
    }
    
    func exchange(){
        swap(&currency1, &currency2)
        updateCurrency()
        
    }
    
    func repeatLive(){
        Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { timer in
            self.live()
            print("Live ")
        }
    }
    
    //MARK: - Network Functions
    func live(){
        manager.live { (response, error) in
            DispatchQueue.main.async{
                if error != nil{
                    print("ERROR ", error!)
                    return
                }
                if response?.success ?? false{
                    guard ((response?.quotes) != nil) else {
                      return
                    }
                    self.currencies = response!.quotes
//                    print(response?.quotes)
                    print("Time Stamp", response)
                    print("Time Stamp", response?.timestamp)
                    self.updateCurrency()
                    print("Come Response")
                }else {
                    print("No Response: ", response!)
                }
                
            }
        }
    }
    
    func list(){
       manager.list { (response, error) in
            DispatchQueue.main.async{
                if error != nil{
                    print("ERROR ", error!)
                    return
                }
                if response?.success ?? false{
                    response?.currencies.forEach({ (key, value) in
                        self.currencyItems.append(CurrencyItem(currencyName: value, currency: key))
                    })
                    print(self.currencyItems)
                    self.setCurrency(currency1: self.currencyItems.first, currency2: self.currencyItems.last)
                }else {
                    print("No Response: ", response!)
                }
                
            }
        }
    }
    
    
}

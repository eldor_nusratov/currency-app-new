//
//  CurrencyItemView.swift
//  CurrencyApp
//
//  Created by eldorbek nusratov on 6/19/20.
//  Copyright © 2020 eldorbek nusratov. All rights reserved.
//

import UIKit

class CurrencyItemView: UIView{
    @IBOutlet var currencyName1 : UILabel!
    @IBOutlet var currencySubtitle1 : UILabel!
    @IBOutlet var currencyPrice1 : UILabel!
    
    @IBOutlet var currencyName2 : UILabel!
    @IBOutlet var currencySubtitle2 : UILabel!
    @IBOutlet var currencyPrice2 : UILabel!
}

//
//  CurrencyEndPoint.swift
//  CurrencyApp
//
//  Created by eldorbek nusratov on 6/19/20.
//  Copyright © 2020 eldorbek nusratov. All rights reserved.
//

import Foundation

enum CurrencyEndPoint : EndPointType {
    case live
    case list
}


extension CurrencyEndPoint{
    
    var baseURL: URL{
        guard let url = URL(string: Constants.BASE_URL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String{
        switch self {
        case .live:
            return Constants.LIVE
        case .list:
            return Constants.LIST
        }
       
    }
    
    var httpMethod: HTTPMethod{
        .get
    }
    
    var task: HTTPTask{
        switch self {
        case .live:
            return .requestParameters(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: [Constants.ACCESS_KEY : Constants.API_KEY])
        case .list:
            return .requestParameters(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: [Constants.ACCESS_KEY : Constants.API_KEY])
        default:
            return .request
        }
    }
    
    var headers: HTTPHeaders?{
        nil
    }
    
    
}

//
//  NetworkManager.swift
//  Freelancer
//
//  Created by eldorbek nusratov on 5/19/20.
//  Copyright © 2020 eldorbek nusratov. All rights reserved.
//

import Foundation



enum NetworkResponse:String {
    case success
    case authenticationError = "You need to be authenticated first."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
}

enum Result<String>{
    case success
    case failure(String)
}


struct NetworkManager {
    
    let router = Router<CurrencyEndPoint>()
    
    func live(completion: @escaping (_ response: CurrencyResponse?, _ error: String?) -> ()) {
        
        router.request(.live) { (data, response, error) in
            
            if error != nil {
                print("Error")
                print(error)
                completion(nil, "Please check your network connection.")
            }
            
            if let response = response as? HTTPURLResponse {
                print("Response data : ", response.statusCode, response.description)
                
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(nil, NetworkResponse.noData.rawValue)
                        return
                    }
                    do {
                        
                        let apiResponse = try JSONDecoder().decode(CurrencyResponse.self, from: responseData)
                        completion(apiResponse, nil)
                    }catch {
                        completion(nil, NetworkResponse.unableToDecode.rawValue)
                    }
                    
                case .failure(_):
                    completion(nil, "\(String(describing: error))")
                }
                
            }
            
        }
    }
    
    func list(completion: @escaping (_ response: CurrencyListResponse?, _ error: String?) -> ()) {
           
           router.request(.list) { (data, response, error) in
               
               if error != nil {
                   print("Error")
                   print(error!)
                   completion(nil, "Please check your network connection.")
               }
               
               if let response = response as? HTTPURLResponse {
                   print("Response data : ", response.statusCode, response.description)
                   
                   let result = self.handleNetworkResponse(response)
                   switch result {
                   case .success:
                       guard let responseData = data else {
                           completion(nil, NetworkResponse.noData.rawValue)
                           return
                       }
                       do {
                           
                           let apiResponse = try JSONDecoder().decode(CurrencyListResponse.self, from: responseData)
                           completion(apiResponse, nil)
                       }catch {
                           completion(nil, NetworkResponse.unableToDecode.rawValue)
                       }
                       
                   case .failure(_):
                       completion(nil, "\(String(describing: error))")
                   }
                   
               }
               
           }
       }
    
    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String>{
        print(response.statusCode)
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .success
        case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
        case 600: return .failure(NetworkResponse.outdated.rawValue)
        default: return .failure(NetworkResponse.failed.rawValue)
        }
    }
}

//
//  NetworkRouter.swift
//  Freelancer
//
//  Created by eldorbek nusratov on 5/21/20.
//  Copyright © 2020 eldorbek nusratov. All rights reserved.
//

import Foundation

public typealias NetworkRouterCompletion = (_ data: Data?,_ response: URLResponse?,_ error: Error?)->()

protocol NetworkRouter: class {
    associatedtype EndPoint: EndPointType
    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion)
    func cancel()
}

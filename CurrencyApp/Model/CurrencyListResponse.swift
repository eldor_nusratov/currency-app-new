//
//  CurrencyListResponse.swift
//  CurrencyApp
//
//  Created by eldorbek nusratov on 6/19/20.
//  Copyright © 2020 eldorbek nusratov. All rights reserved.
//

import Foundation


struct CurrencyListResponse {
    var success: Bool
    var terms: String
    var privacy: String
    var currencies : [String : String]
}

extension CurrencyListResponse : Decodable{
    
    private enum RegisterApiResponseCodingKeys: String, CodingKey {
        case success = "success"
        case terms = "terms"
        case privacy = "privacy"
        case currencies = "currencies"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RegisterApiResponseCodingKeys.self)

        success = try container.decode(Bool.self, forKey: .success)
        terms = try container.decode(String.self, forKey: .terms)
        privacy = try container.decode(String.self, forKey: .privacy)
        currencies = try container.decode(Dictionary.self, forKey: .currencies)
    }
    
}


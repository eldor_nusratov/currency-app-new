//
//  CurrencyResponse.swift
//  CurrencyApp
//
//  Created by eldorbek nusratov on 6/19/20.
//  Copyright © 2020 eldorbek nusratov. All rights reserved.
//

import Foundation

struct CurrencyResponse {
    var success: Bool
    var terms: String
    var privacy: String
    var timestamp: Int
    var source: String
    var quotes : [String : Double]
}

extension CurrencyResponse : Decodable{
    
    private enum RegisterApiResponseCodingKeys: String, CodingKey {
        case success = "success"
        case terms = "terms"
        case privacy = "privacy"
        case timestamp = "timestamp"
        case source = "source"
        case quotes = "quotes"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RegisterApiResponseCodingKeys.self)

        success = try container.decode(Bool.self, forKey: .success)
        terms = try container.decode(String.self, forKey: .terms)
        privacy = try container.decode(String.self, forKey: .privacy)
        timestamp = try container.decode(Int.self, forKey: .timestamp)
        source = try container.decode(String.self, forKey: .source)
        quotes = try container.decode(Dictionary.self, forKey: .quotes)
    }
    
}


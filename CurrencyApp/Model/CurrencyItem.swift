//
//  CurrencyItem.swift
//  CurrencyApp
//
//  Created by eldorbek nusratov on 6/19/20.
//  Copyright © 2020 eldorbek nusratov. All rights reserved.
//

import Foundation

struct CurrencyItem {
    var currencyName : String
    var currency : String
}
